package me.factral.factralplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class FactralPlugin extends JavaPlugin
    {

    @Override
    public void onEnable()
        {
        loadConfig();
        getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&l[&a&lFactral Plugin&7&l] &7Plugin For Factral Server has been enabled"));
        Objects.requireNonNull(this.getCommand(cmd1)).setExecutor(this);

        }

    @Override
    public void onDisable()
        {
        getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&l[&a&lFactral Plugin&7&l] &cPlugin For Factral Server has been disabled"));
        saveConfig();
        }

    public void loadConfig()
        {
        if (!getDataFolder().exists())
            {
            getConfig().options().copyDefaults(true);
            this.saveDefaultConfig();

            }

        }


    public String cmd1 = "rankdesc";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
        {


        if (cmd.getName().equalsIgnoreCase(cmd1))
            {
            try
                {
                if (args.length < 2)
                    {


                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "=============================="));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&a&lCOMMANDS:"));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&6/rd create <name> - &7create a " +
                            "new " + "desc."));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&6/rd addline <name> <text> - " +
                            "&7adds a line of " + "text to your desc."));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&6/rd removeline <name> <Line #> -" +
                            " " + "&7remove a " + "line of text"));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&6/rd send <player> <desc> - " + "&7sends " +
                            "desc. to player"));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&6/rd delete <name> -&7 Deletes " +
                            "desc."));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "&6/rd admin reload - &7Reloads " +
                            "the config.yml"));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + "=============================="));
                    return false;


                    }
                else if (args[0].equalsIgnoreCase("create"))
                    {

                    if (sender.hasPermission("rankdesc.create"))
                        {


                        if (!Objects.requireNonNull(getConfig().getConfigurationSection("Descriptions")).contains(args[1]))
                            {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.Created")));
                            onCreate(args[1]);
                            saveConfig();

                            }
                        }
                    else
                        {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.noPerm")));
                        }


                    }
                else if (args[0].equalsIgnoreCase("addline"))
                    {
                    if (getConfig().getConfigurationSection("Descriptions").contains(args[1]))
                        {
                        if (sender.hasPermission("rankdesc.addline"))
                            {

                            String name = args[1];

                            if (args.length < 3)
                                {
                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.InvalidUsage")));
                                return false;
                                }
                            else
                                {

                                String[] temp = Arrays.copyOfRange(args, 2, args.length);
                                String temp2 = temp[0];

                                for (int i = 1; i < temp.length; i++)
                                    {

                                    temp2 += " " + temp[i];

                                    }


                                writeToConfig(temp2, args[1]);
                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.LineAdded")));


                                }
                            /*List<String> lines = getConfig().getStringList("Descriptions");
                            lines.add(temp2);
                            getConfig().set("Descriptions." + args[1], lines);
                            saveConfig();
                             */
                            }
                        else
                            {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.noPerm")));
                            }

                        }
                    else
                        {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.notExists")));
                        }
                    }
                else if (args[0].equalsIgnoreCase("admin"))
                    {
                    if (args[1].equalsIgnoreCase("reload"))
                        {
                        if (sender.hasPermission("rankdesc.reload"))
                            {
                            reloadConfig();
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.Reloaded")));
                            }
                        else
                            {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.noPerm")));
                            }
                        }
                    else
                        {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.InvalidUsage")));
                        }
                    }
                else if (args[0].equalsIgnoreCase("send"))
                    {

                    String name = args[2];


                    if ((getServer().getPlayer(args[1])) != null)
                        {


                        if (!Objects.requireNonNull(getConfig().getConfigurationSection("Descriptions")).contains(args[2]))
                            {

                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.notExists")));

                            }
                        else
                            {
                            List<String> texts = getConfig().getStringList("Descriptions." + name);
                            int sizeOfTexts = getConfig().getStringList("Descriptions." + name).size();

                            for (int i = 0; i < sizeOfTexts; i++)
                                {
                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', texts.get(i)));
                                }

                            }

                        }
                    else
                        {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages" +
                                ".Prefix") + " &cPlayer " + args[1] + " is not online!"));
                        return false;
                        }
                    }
                else if (args[0].equalsIgnoreCase("removeline"))
                    {

                    // /rd removeLine <name> <line Number>
                    if(sender.hasPermission("rankdesc.removeline"))
                        {
                        if(Objects.requireNonNull(getConfig().getConfigurationSection("Descriptions")).contains(args[1]))
                            {
                            List<String> lines = getConfig().getStringList("Descriptions." + args[1]);
                            int number = Integer.parseInt(args[2]);
                            int size = lines.size();
                            if(number > size)
                                {
                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString(
                                        "Messages.Prefix") + " " + getConfig().getString("Messages.lineRange") + "&e" +
                                        "(Size: " + size));
                                return false;
                                }
                            else
                                {
                                lines.remove(number);
                                getConfig().set("Descriptions." + args[1], lines);
                                saveConfig();
                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&',getConfig().getString(
                                        "Messages.Prefix") + " " + getConfig().getString("Messages.lineRemoved") + " " +
                                        "&7(" + number + ")."));
                                }

                            }
                        else
                            {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.notExists")));
                            }
                        }
                    else
                        {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.noPerm")));
                        }
                    } // /rd delete <name>
                else if(args[0].equalsIgnoreCase("delete"))
                    {
                    if(sender.hasPermission("rankdesc.delete"))
                        {
                        if(Objects.requireNonNull(getConfig().getConfigurationSection("Descriptions")).contains(args[1]))
                            {
                            getConfig().set("Descriptions." + args[1], null);
                            saveConfig();
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString(
                                    "Messages.Prefix") + " " + getConfig().getString("Messages.descDeleted")));

                            }
                        else
                            {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.notExists")));
                            }
                        }
                    else
                        {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.noPerm")));
                        }
                    }

                } catch (Exception e)
                {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.Prefix") + " " + getConfig().getString("Messages.Exception")));
                }
            }


        return true;
        }


    public void onCreate(String name)
        {
        Objects.requireNonNull(getConfig().getConfigurationSection("Descriptions")).createSection(name);
        }


    public void writeToConfig(String temp2, String name)
        {
        /*List<String> lines = getConfig().getStringList("Descriptions");
        lines.add(temp2, );
        getConfig().set("Descriptions." + name, lines);
        saveConfig();


         */

        List<String> lines = getConfig().getStringList("Descriptions." + name);
        lines.add(temp2);
        getConfig().set("Descriptions." + name, lines);
        saveConfig();


        }


    }


